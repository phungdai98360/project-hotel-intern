const { Sequelize } = require("sequelize");
var Rooms = sequelize.define(
  "rooms",
  {
    code_room: {
      type: Sequelize.STRING(255),
      field: "code_room",
      primaryKey: true,
      // autoIncrement: true,
      allowNull: false,
    },
    floor: {
      type: Sequelize.BIGINT,
      field: "floor",
      allowNull: true,
    },
    createdAt: {
      type: Sequelize.DATE,
      field: "createdAt",
      allowNull: true,
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: "updatedAt",
      allowNull: true,
    },
  },
  {
    freezeTableName: true,
    indexes: [],
  },
);
module.exports = Rooms;

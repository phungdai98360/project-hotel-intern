const { Sequelize } = require("sequelize");
var DetailPriceServices = sequelize.define(
  "detail_price_services",
  {
    id: {
      type: Sequelize.BIGINT,
      field: "id",
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    price: {
      type: Sequelize.BIGINT,
      field: "price",
      allowNull: true,
    },
    date_apply: {
      type: Sequelize.DATE,
      field: "date_apply",
      allowNull: true,
    },
    createdAt: {
      type: Sequelize.DATE,
      field: "createdAt",
      allowNull: true,
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: "updatedAt",
      allowNull: true,
    },
  },
  {
    freezeTableName: true,
    indexes: [],
  },
);
module.exports = DetailPriceServices;

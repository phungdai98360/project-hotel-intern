const { Sequelize } = require("sequelize");
var DetailServices = sequelize.define(
  "detail_services",
  {
    id: {
      type: Sequelize.BIGINT,
      field: "id",
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    amount: {
      type: Sequelize.BIGINT,
      field: "amount",
      //unique: true,
      allowNull: true,
    },
    price: {
      type: Sequelize.BIGINT,
      field: "price",
      allowNull: false,
    },
    payed: {
      type: Sequelize.BOOLEAN,
      field: "payed",
      allowNull: true,
    },
    createdAt: {
      type: Sequelize.DATE,
      field: "createdAt",
      allowNull: true,
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: "updatedAt",
      allowNull: true,
    },
  },
  {
    freezeTableName: true,
    indexes: [],
  },
);
module.exports = DetailServices;

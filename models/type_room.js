const { Sequelize } = require("sequelize");
var TypeRooms = sequelize.define(
  "type_rooms",
  {
    code_type: {
      type: Sequelize.STRING(255),
      field: "code_type",
      primaryKey: true,
      // autoIncrement: true,
      allowNull: false,
    },
    name: {
      type: Sequelize.STRING(255),
      field: "name",
      allowNull: true,
    },
    createdAt: {
      type: Sequelize.DATE,
      field: "createdAt",
      allowNull: true,
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: "updatedAt",
      allowNull: true,
    },
  },
  {
    freezeTableName: true,
    indexes: [],
  },
);
module.exports = TypeRooms;

const { Sequelize } = require("sequelize");
var Sales = sequelize.define(
  "sales",
  {
    code_sale: {
      type: Sequelize.BIGINT,
      field: "code_sale",
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    name: {
      type: Sequelize.STRING(255),
      field: "name",
      allowNull: true,
    },
    date_start: {
      type: Sequelize.DATE,
      field: "date_start",
      allowNull: true,
    },
    date_end: {
      type: Sequelize.DATE,
      field: "date_end",
      allowNull: true,
    },
    createdAt: {
      type: Sequelize.DATE,
      field: "createdAt",
      allowNull: true,
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: "updatedAt",
      allowNull: true,
    },
  },
  {
    freezeTableName: true,
    indexes: [],
  },
);
module.exports = Sales;

const { Sequelize } = require("sequelize");
var RentTickets = sequelize.define(
  "rent_tickets",
  {
    code_rent: {
      type: Sequelize.STRING(255),
      field: "code_rent",
      primaryKey: true,
      // autoIncrement: true,
      allowNull: false,
    },
    time_welcome: {
      type: Sequelize.DATE,
      field: "time_welcome",
      allowNull: true,
    },
    payed: {
      type: Sequelize.BOOLEAN,
      field: "payed",
      allowNull: true,
    },
    date_leave: {
      type: Sequelize.DATE,
      field: "date_leave",
      allowNull: true,
    },
    createdAt: {
      type: Sequelize.DATE,
      field: "createdAt",
      allowNull: true,
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: "updatedAt",
      allowNull: true,
    },
  },
  {
    freezeTableName: true,
    indexes: [],
  },
);
module.exports = RentTickets;

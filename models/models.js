global.Sequelize = require("sequelize");
global.sequelize = new Sequelize("hotel_manager_v1", "root", "mixaolanot123", {
  host: "127.0.0.1",
  //logging: false,
  dialect: "mysql",
  pool: {
    max: 50,
    min: 0,
    idle: 10000,
  },
});

// TABLES

global.Part = require("./part.js");
global.Customers = require("./customers");
global.Bills = require("./bills");
global.DetailBills = require("./detail_bills");
global.DetailCustomerAts = require("./detail_customer_ats");
global.DetailOrderTickets = require("./detail_order_tickets");
global.DetailSales = require("./detail_sales");
global.DetailServices = require("./detail_serviec");
global.KindRooms = require("./kind_room");
global.OrderTickets = require("./order_ticket");
global.RankRooms = require("./rank_room");
global.Rents = require("./rents");
global.RentTickets = require("./rent_tickets");
global.Rooms = require("./rooms");
global.Sales = require("./sales");
global.Services = require("./services");
global.Staffs = require("./staffs");
global.StatusRooms = require("./status_room");
global.TypeRooms = require("./type_room");
global.DetailStatus = require("./detail_status");
global.DetailPriceRooms = require("./detail_price_rooms");
global.DetailPriceServices = require("./detail_price_services");
global.HistoryExels = require("./history_exels");
global.CustomerExels=require("./customer_exels");
//join detail order
OrderTickets.hasMany(DetailOrderTickets, {
  sourceKey: "code_order",
  foreignKey: "order_tickets_code_order",
  as: "detail_list_detail_order",
});
DetailOrderTickets.belongsTo(OrderTickets, {
  targetKey: "code_order",
  foreignKey: "order_tickets_code_order",
  as: "detail_info_order",
});
RankRooms.hasMany(DetailOrderTickets, {
  sourceKey: "id",
  foreignKey: "rank_rooms_id",
  as: "detail_list_detail_order",
});
DetailOrderTickets.belongsTo(RankRooms, {
  targetKey: "id",
  foreignKey: "rank_rooms_id",
  as: "detail_info_rank_room",
});
//join rank room
KindRooms.hasMany(RankRooms, {
  sourceKey: "code_kind",
  foreignKey: "kind_rooms_code_kind",
  as: "detail_list_rank_room",
});
RankRooms.belongsTo(KindRooms, {
  targetKey: "code_kind",
  foreignKey: "kind_rooms_code_kind",
  as: "detail_info_kind_room",
});
TypeRooms.hasMany(RankRooms, {
  sourceKey: "code_type",
  foreignKey: "type_rooms_code_type",
  as: "detail_list_rank_room_type",
});
RankRooms.belongsTo(TypeRooms, {
  targetKey: "code_type",
  foreignKey: "type_rooms_code_type",
  as: "detail_info_type_room",
});
//join customer ,order,staff
Customers.hasMany(OrderTickets, {
  sourceKey: "code_customer",
  foreignKey: "customers_code_customer",
  as: "detail_list_order",
});
OrderTickets.belongsTo(Customers, {
  targetKey: "code_customer",
  foreignKey: "customers_code_customer",
  as: "detail_info_customer",
});
Staffs.hasMany(OrderTickets, {
  sourceKey: "code_staff",
  foreignKey: "staffs_code_staff",
  as: "detail_list_order",
});
OrderTickets.belongsTo(Staffs, {
  targetKey: "code_staff",
  foreignKey: "staffs_code_staff",
  as: "detail_info_staff",
});
//join staff ,part
Part.hasMany(Staffs, {
  sourceKey: "code_part",
  foreignKey: "parts_code_part",
  as: "detail_list_staff",
});
Staffs.belongsTo(Part, {
  targetKey: "code_part",
  foreignKey: "parts_code_part",
  as: "detail_info_part",
});
//Rooms
RankRooms.hasMany(Rooms, {
  sourceKey: "id",
  foreignKey: "rank_rooms_id",
  as: "detail_list_room",
});
Rooms.belongsTo(RankRooms, {
  targetKey: "id",
  foreignKey: "rank_rooms_id",
  as: "detail_info_rankroom",
});
//detail room
Rooms.hasMany(DetailStatus, {
  sourceKey: "code_room",
  foreignKey: "rooms_code_room",
  as: "detail_list_status",
});
DetailStatus.belongsTo(Rooms, {
  targetKey: "code_room",
  foreignKey: "rooms_code_room",
  as: "detail_info_room",
});
StatusRooms.hasMany(DetailStatus, {
  sourceKey: "code_status",
  foreignKey: "status_rooms_code_status",
  as: "detail_list_detailstatus",
});
DetailStatus.belongsTo(StatusRooms, {
  targetKey: "code_status",
  foreignKey: "status_rooms_code_status",
  as: "detail_info_status",
});
OrderTickets.hasMany(DetailStatus, {
  sourceKey: "code_order",
  foreignKey: "order_tickets_code_order",
  as: "detail_list_detailstatus",
});
DetailStatus.belongsTo(OrderTickets, {
  targetKey: "code_order",
  foreignKey: "order_tickets_code_order",
  as: "detail_info_order",
});
//rent-ticket
OrderTickets.hasOne(RentTickets, {
  sourceKey: "code_order",
  foreignKey: "order_tickets_code_order",
  as: "detail_list_rent",
});
RentTickets.belongsTo(OrderTickets, {
  targetKey: "code_order",
  foreignKey: "order_tickets_code_order",
  as: "detail_info_order",
});
Staffs.hasMany(RentTickets, {
  sourceKey: "code_staff",
  foreignKey: "staffs_code_staff",
  as: "detail_list_rent",
});
RentTickets.belongsTo(Staffs, {
  targetKey: "code_staff",
  foreignKey: "staffs_code_staff",
  as: "detail_info_staff",
});
Customers.hasMany(RentTickets, {
  sourceKey: "code_customer",
  foreignKey: "customers_code_customer",
  as: "detail_list_rent",
});
RentTickets.belongsTo(Customers, {
  targetKey: "code_customer",
  foreignKey: "customers_code_customer",
  as: "detail_info_customer",
});
//rent
RentTickets.hasMany(Rents, {
  sourceKey: "code_rent",
  foreignKey: "rent_tickets_code_rent",
  as: "detail_list_rents",
});
Rents.belongsTo(RentTickets, {
  targetKey: "code_rent",
  foreignKey: "rent_tickets_code_rent",
  as: "detail_info_rent_ticket",
});
Rooms.hasMany(Rents, {
  sourceKey: "code_room",
  foreignKey: "rooms_code_room",
  as: "detail_list_rents",
});
Rents.belongsTo(Rooms, {
  targetKey: "code_room",
  foreignKey: "rooms_code_room",
  as: "detail_info_rooms",
});
Bills.hasMany(Rents, {
  sourceKey: "code_bill",
  foreignKey: "bills_code_bill",
  as: "detail_list_rents",
});
Rents.belongsTo(Bills, {
  targetKey: "code_bill",
  foreignKey: "bills_code_bill",
  as: "detail_info_bill",
});
//detail customer at
Customers.hasMany(DetailCustomerAts, {
  sourceKey: "code_customer",
  foreignKey: "customers_code_customer",
  as: "detail_list_detail_rent",
});
DetailCustomerAts.belongsTo(Customers, {
  targetKey: "code_customer",
  foreignKey: "customers_code_customer",
  as: "detail_info_customer",
});
Rents.hasMany(DetailCustomerAts, {
  sourceKey: "id",
  foreignKey: "detail_rents_id",
  as: "detail_list_detail_rent",
});
DetailCustomerAts.belongsTo(Rents, {
  targetKey: "id",
  foreignKey: "detail_rents_id",
  as: "detail_info_rent",
});
//detal service
Services.hasMany(DetailServices, {
  sourceKey: "code",
  foreignKey: "services_code",
  as: "detail_list_detail_service",
});
DetailServices.belongsTo(Services, {
  targetKey: "code",
  foreignKey: "services_code",
  as: "detail_info_service",
});
Rents.hasMany(DetailServices, {
  sourceKey: "id",
  foreignKey: "detail_rents_id",
  as: "detail_list_detail_service",
});
DetailServices.belongsTo(Rents, {
  targetKey: "id",
  foreignKey: "detail_rents_id",
  as: "detail_info_rent",
});
Bills.hasMany(DetailServices, {
  sourceKey: "code_bill",
  foreignKey: "bills_code_bill",
  as: "detail_list_detail_service",
});
DetailServices.belongsTo(Bills, {
  targetKey: "code_bill",
  foreignKey: "bills_code_bill",
  as: "detail_info_bill",
});
//detail sale
Sales.hasMany(DetailSales, {
  sourceKey: "code_sale",
  foreignKey: "sales_code_sale",
  as: "detail_list_detail_sale",
});
DetailSales.belongsTo(Sales, {
  targetKey: "code_sale",
  foreignKey: "sales_code_sale",
  as: "detail_info_sale",
});
RankRooms.hasMany(DetailSales, {
  sourceKey: "id",
  foreignKey: "rank_rooms_id",
  as: "detail_list_detail_sale",
});
DetailSales.belongsTo(RankRooms, {
  targetKey: "id",
  foreignKey: "rank_rooms_id",
  as: "detail_info_rank_room",
});
//bill
Staffs.hasMany(Bills, {
  sourceKey: "code_staff",
  foreignKey: "staffs_code_staff",
  as: "detail_list_detail_bill",
});
Bills.belongsTo(Staffs, {
  targetKey: "code_staff",
  foreignKey: "staffs_code_staff",
  as: "detail_info_staff",
});
RentTickets.hasMany(Bills, {
  sourceKey: "code_rent",
  foreignKey: "rent_tickets_code_rent",
  as: "detail_list_detail_bill",
});
Bills.belongsTo(RentTickets, {
  targetKey: "code_rent",
  foreignKey: "rent_tickets_code_rent",
  as: "detail_info_rent_ticket",
});
//price room
RankRooms.hasMany(DetailPriceRooms, {
  sourceKey: "id",
  foreignKey: "rank_rooms_id",
  as: "detail_list_detail_price",
});
DetailPriceRooms.belongsTo(RankRooms, {
  targetKey: "id",
  foreignKey: "rank_rooms_id",
  as: "detail_info_rank_room",
});
//price service
Services.hasMany(DetailPriceServices, {
  sourceKey: "code",
  foreignKey: "services_code",
  as: "detail_list_detail_price",
});
DetailPriceServices.belongsTo(Services, {
  targetKey: "code",
  foreignKey: "services_code",
  as: "detail_info_service",
});

const { Sequelize } = require("sequelize");
var Rents = sequelize.define(
  "detail_rents",
  {
    id: {
      type: Sequelize.BIGINT,
      field: "id",
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    payed: {
      type: Sequelize.BOOLEAN,
      field: "payed",
      allowNull: true,
    },
    date_leave: {
      type: Sequelize.DATE,
      field: "date_leave",
      allowNull: true,
    },
    surcharge: {
      type: Sequelize.BIGINT,
      field: "surcharge",
      allowNull: true,
    },
    createdAt: {
      type: Sequelize.DATE,
      field: "createdAt",
      allowNull: true,
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: "updatedAt",
      allowNull: true,
    },
  },
  {
    freezeTableName: true,
    indexes: [],
  },
);
module.exports = Rents;

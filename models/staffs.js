const { Sequelize } = require("sequelize");
var Staffs = sequelize.define(
  "staffs",
  {
    code_staff: {
      type: Sequelize.STRING(10),
      field: "code_staff",
      primaryKey: true,
      // autoIncrement: true,
      allowNull: false,
    },
    identy_card: {
      type: Sequelize.STRING(11),
      field: "identy_card",
      unique: true,
      allowNull: false,
    },
    firt_name: {
      type: Sequelize.STRING(20),
      field: "firt_name",
      allowNull: true,
    },
    last_name: {
      type: Sequelize.STRING(20),
      field: "last_name",
      allowNull: true,
    },
    gender: {
      type: Sequelize.BOOLEAN,
      field: "gender",
      allowNull: true,
    },
    address: {
      type: Sequelize.STRING(255),
      field: "address",
      allowNull: true,
    },
    phone_number: {
      type: Sequelize.STRING(15),
      field: "phone_number",
      allowNull: true,
    },
    email: {
      type: Sequelize.STRING(30),
      field: "email",
      allowNull: true,
    },
    password: {
      type: Sequelize.STRING(255),
      field: "password",
      allowNull: true,
    },
    role: {
      type: Sequelize.STRING(10),
      field: "role",
      allowNull: true,
    },
    createdAt: {
      type: Sequelize.DATE,
      field: "createdAt",
      allowNull: true,
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: "updatedAt",
      allowNull: true,
    },
    deleted: {
      type: Sequelize.BOOLEAN,
      field: "deleted",
      allowNull: true,
    },
  },
  {
    freezeTableName: true,
    indexes: [],
  },
);
module.exports = Staffs;

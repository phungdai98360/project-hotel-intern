const { Sequelize } = require("sequelize");
var KindRooms = sequelize.define(
  "kind_rooms",
  {
    code_kind: {
      type: Sequelize.STRING(255),
      field: "code_kind",
      primaryKey: true,
      allowNull: false,
    },
    name: {
      type: Sequelize.STRING(255),
      field: "name",
      allowNull: true,
    },
    createdAt: {
      type: Sequelize.DATE,
      field: "createdAt",
      allowNull: true,
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: "updatedAt",
      allowNull: true,
    },
  },
  {
    freezeTableName: true,
    indexes: [],
  },
);
module.exports = KindRooms;

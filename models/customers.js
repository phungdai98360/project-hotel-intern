const { Sequelize } = require("sequelize");
var Customers = sequelize.define(
  "customers",
  {
    code_customer: {
      type: Sequelize.BIGINT,
      field: "code_customer",
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    code: {
      type: Sequelize.STRING(255),
      field: "code",
      unique: true,
      allowNull: false,
    },
    firt_name: {
      type: Sequelize.STRING(255),
      field: "firt_name",
      allowNull: true,
    },
    last_name: {
      type: Sequelize.STRING(255),
      field: "last_name",
      allowNull: true,
    },
    gender: {
      type: Sequelize.BOOLEAN,
      field: "gender",
      allowNull: true,
    },
    address: {
      type: Sequelize.STRING(255),
      field: "address",
      allowNull: true,
    },
    phone_number: {
      type: Sequelize.STRING(255),
      field: "phone_number",
      allowNull: true,
    },
    email: {
      type: Sequelize.STRING(255),
      field: "email",
      allowNull: true,
    },
    point: {
      type: Sequelize.BIGINT,
      field: "point",
      allowNull: true,
    },
    createdAt: {
      type: Sequelize.DATE,
      field: "createdAt",
      allowNull: true,
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: "updatedAt",
      allowNull: true,
    },
  },
  {
    freezeTableName: true,
    indexes: [],
  },
);
module.exports = Customers;

const { Sequelize } = require("sequelize");
var Bills = sequelize.define(
  "bills",
  {
    code_bill: {
      type: Sequelize.STRING(255),
      field: "code_bill",
      primaryKey: true,
      allowNull: false,
    },
    price_room: {
      type: Sequelize.BIGINT,
      field: "price_room",
      allowNull: true,
    },
    price_service: {
      type: Sequelize.BIGINT,
      field: "price_service",
      allowNull: true,
    },
    total_price: {
      type: Sequelize.BIGINT,
      field: "total_price",
      allowNull: true,
    },
    createdAt: {
      type: Sequelize.DATE,
      field: "createdAt",
      allowNull: true,
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: "updatedAt",
      allowNull: true,
    },
  },
  {
    freezeTableName: true,
    indexes: [],
  },
);
module.exports = Bills;

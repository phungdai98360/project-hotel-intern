const {Sequelize}=require("sequelize");
var DetailOrderTickets=sequelize.define('detail_order_tickets',{
    id: {
        type: Sequelize.BIGINT,
        field: 'id',
        primaryKey: true,
		autoIncrement: true,
		allowNull: false,
    },
    amount_room: {
		type: Sequelize.BIGINT,
		field: 'amount_room',
		allowNull: false,
	},
	createdAt:{
		type:Sequelize.DATE,
		field:"createdAt",
		allowNull: true,
	},
	updatedAt:{
		type:Sequelize.DATE,
		field:"updatedAt",
		allowNull: true,
	},
},{
    freezeTableName: true,
	indexes: []
});
module.exports=DetailOrderTickets;
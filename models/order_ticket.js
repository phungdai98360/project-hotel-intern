const { Sequelize } = require("sequelize");
var OrderTickets = sequelize.define(
  "order_tickets",
  {
    code_order: {
      type: Sequelize.BIGINT,
      field: "code_order",
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    date_welcome: {
      type: Sequelize.DATEONLY,
      field: "date_welcome",
      allowNull: true,
    },
    date_leave: {
      type: Sequelize.DATE,
      field: "date_leave",
      allowNull: true,
    },
    status: {
      type: Sequelize.STRING(255),
      field: "status",
      allowNull: true,
    },
    createdAt: {
      type: Sequelize.DATE,
      field: "createdAt",
      allowNull: true,
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: "updatedAt",
      allowNull: true,
    },
  },
  {
    freezeTableName: true,
    indexes: [],
  },
);
module.exports = OrderTickets;

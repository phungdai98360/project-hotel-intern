const { Sequelize } = require("sequelize");
var StatusRooms = sequelize.define(
  "status_rooms",
  {
    code_status: {
      type: Sequelize.STRING(255),
      field: "code_status",
      primaryKey: true,
      // autoIncrement: true,
      allowNull: false,
    },
    name: {
      type: Sequelize.STRING(255),
      field: "name",
      allowNull: true,
    },
    createdAt: {
      type: Sequelize.DATE,
      field: "createdAt",
      allowNull: true,
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: "updatedAt",
      allowNull: true,
    },
  },
  {
    freezeTableName: true,
    indexes: [],
  },
);
module.exports = StatusRooms;

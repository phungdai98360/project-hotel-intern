const { Sequelize } = require("sequelize");
var RankRooms = sequelize.define(
  "rank_rooms",
  {
    id: {
      type: Sequelize.BIGINT,
      field: "id",
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    url_image: {
      type: Sequelize.STRING(500),
      field: "url_image",
      allowNull: true,
    },
    limit_person: {
      type: Sequelize.BIGINT,
      field: "limit_person",
      allowNull: true,
    },
    createdAt: {
      type: Sequelize.DATE,
      field: "createdAt",
      allowNull: true,
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: "updatedAt",
      allowNull: true,
    },
  },
  {
    freezeTableName: true,
    indexes: [],
  },
);
module.exports = RankRooms;

const { Sequelize } = require("sequelize");
var HistoryExels = sequelize.define(
  "history_exels",
  {
    id: {
      type: Sequelize.BIGINT,
      field: "id",
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    identity_card: {
      type: Sequelize.STRING(40),
      field: "identity_card",
      allowNull: false,
    },
    rank_room: {
      type: Sequelize.BIGINT,
      field: "rank_room",
      allowNull: false,
    },
    date_welcome: {
      type: Sequelize.DATEONLY,
      field: "date_welcome",
      allowNull: true,
    },
    date_leave: {
      type: Sequelize.DATEONLY,
      field: "date_leave",
      allowNull: true,
    },
    createdAt: {
      type: Sequelize.DATE,
      field: "createdAt",
      allowNull: true,
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: "updatedAt",
      allowNull: true,
    },
  },
  {
    freezeTableName: true,
    indexes: [],
  },
);
module.exports = HistoryExels;

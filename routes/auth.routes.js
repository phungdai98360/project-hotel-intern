const { verifySignUp, authJwt } = require("../middleware");
const controller = require("../controller/auth.controller");
const roomController = require("../controller/rooms.controller");
const orderController = require("../controller/order.controller");
const rentController = require("../controller/rent.controller");
const billController = require("../controller/bill.controller");
module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept",
    );
    next();
  });

  app.post(
    "/api/auth/signup",
    [verifySignUp.checkDuplicateUsernameOrEmail],
    controller.signup,
  );

  app.post("/api/auth/login", controller.login);
  app.get(
    "/api/auth/get-order-room/:id",
    [authJwt.verifyToken],
    controller.getRoomRandom,
  );
  app.post(
    "/api/auth/create-room",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.createRoom,
  );
  app.post(
    "/api/auth/create-rent-ticket",
    [authJwt.verifyToken],
    controller.createRentTicket,
  );
  app.post("/api/auth/give-room", [authJwt.verifyToken], controller.giveRooms);
  app.post(
    "/api/auth/detail-customer-at",
    [authJwt.verifyToken],
    controller.detailCustomerAt,
  );
  app.post(
    "/api/auth/create-rent-ticket-by-customer",
    [authJwt.verifyToken],
    controller.createRentTicketByCustomer,
  );
  app.get(
    "/api/auth/get-all-service",
    [authJwt.verifyToken],
    controller.getAllService,
  );

  app.post(
    "/api/auth/create-service",
    [authJwt.verifyToken],
    controller.createService,
  );
  app.put(
    "/api/auth/update-service",
    [authJwt.verifyToken],
    controller.updateService,
  );
  app.post(
    "/api/auth/create-detail-service",
    [authJwt.verifyToken],
    controller.createDetailService,
  );
  app.get(
    "/api/auth/get-all-staff",
    [authJwt.verifyToken],
    controller.getAllStaff,
  );
  app.put(
    "/api/auth/get-all-staff",
    [authJwt.verifyToken],
    controller.updateStaff,
  );
  app.put(
    "/api/auth/get-all-staff-delete",
    [authJwt.verifyToken],
    controller.deleteStaff,
  );
  app.get("/api/auth/get-all-part", [authJwt.verifyToken], controller.getPart);
  app.put(
    "/api/auth/change-room",
    [authJwt.verifyToken],
    controller.changeRoom,
  );
  app.get(
    "/api/auth/check-change-room",
    [authJwt.verifyToken],
    controller.checkChangeRom,
  );
  app.get(
    "/api/auth/revenue",
    [authJwt.verifyToken],
    controller.revenueFromDateToDate,
  );
  app.get(
    "/api/auth/dasdboard-bill",
    [authJwt.verifyToken],
    controller.dashboardBill,
  );
  //order and rent
  app.get(
    "/api/auth/order-ticket",
    [authJwt.verifyToken],
    orderController.getOrderTicket,
  );
  app.get(
    "/api/auth/order-ticket/:id",
    [authJwt.verifyToken],
    orderController.getOrderTicketById,
  );
  app.put(
    "/api/auth/order-ticket/:id",
    [authJwt.verifyToken],
    orderController.acceptOrderTicket,
  );
  app.put(
    "/api/auth/cancel-order-ticket/",
    [authJwt.verifyToken],
    orderController.cancelOrder,
  );
  app.delete(
    "/api/auth/order-ticket",
    [authJwt.verifyToken],
    orderController.deleteOrderTicket,
  );
  app.get(
    "/api/auth/order-ticket-by-customer",
    [authJwt.verifyToken],
    orderController.getOrderTicketFromCustomer,
  );

  app.get(
    "/api/auth/detail-rent",
    [authJwt.verifyToken],
    rentController.getDetailRent,
  );
  app.get(
    "/api/auth/get-customer-at-room",
    [authJwt.verifyToken],
    rentController.getRoomByDeatilRent,
  );
  app.get(
    "/api/auth/get-limit-person",
    [authJwt.verifyToken],
    rentController.getLimitPerson,
  );
  app.get(
    "/api/auth/get-customer-being-at",
    [authJwt.verifyToken],
    rentController.getCustomerBeingAt,
  );
  app.post("/api/auth/create-qr-code", orderController.createQrCode);
  app.put(
    "/api/auth/detail-status",
    [authJwt.verifyToken],
    rentController.updateDetailStatus,
  );
  //bill
  app.get(
    "/api/auth/check-room-to-bill",
    [authJwt.verifyToken],
    billController.checkRoomToBills,
  );
  app.get(
    "/api/auth/calculate-room",
    [authJwt.verifyToken],
    billController.calculateRooms,
  );
  app.post(
    "/api/auth/create-bill",
    [authJwt.verifyToken],
    billController.createBill,
  );
  app.get("/api/auth/bills", [authJwt.verifyToken], billController.getBills);
  //renk room
  app.get(
    "/api/auth/rank-room",
    [authJwt.verifyToken],
    roomController.getRankRoom,
  );
  app.post(
    "/api/auth/rank-room",
    [authJwt.verifyToken],
    roomController.createRankRoom,
  );
  app.post(
    "/api/auth/price-rank-room",
    [authJwt.verifyToken],
    roomController.createPriceRoom,
  );
};

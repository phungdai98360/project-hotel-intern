"use strict";
var express = require("express");
var router = express.Router();
router.get("/getall",(req,res)=>{
    Services.findAll().then(service=>{
        res.json({
            total:service.length,
            data:service
        })
    })
})
module.exports = router;
var QRCode = require("qrcode");
var nodemailer = require("nodemailer");
const { QueryTypes } = require("sequelize");
const Op = Sequelize.Op;
const option = {
  service: "gmail",
  auth: {
    user: "mailsenderptithcm@gmail.com", // email hoặc username
    pass: "ptithcm123", // password
  },
};
var transporter = nodemailer.createTransport(option);
transporter.verify(function (error, success) {
  // Nếu có lỗi.
  if (error) {
    console.log(error);
  } else {
    //Nếu thành công.
    console.log("Kết nối thành công!");
  }
});
exports.getOrderTicket = (req, res) => {
  let { page, perPage, date_welcome, status } = req.query;
  let where = {
    date_welcome:date_welcome ,
    status: status,
  };
  if (!date_welcome) {
    delete where.date_welcome;
  }
  if (!status) {
    delete where.status;
  }
  OrderTickets.findAndCountAll({
    limit: perPage ? parseInt(perPage) : 10,
    offset: page ? parseInt(page - 1) * parseInt(perPage) : 0,
    distinct: true,
    attributes: [
      "code_order",
      "date_welcome",
      "date_leave",
      "status",
      "staffs_code_staff",
      "createdAt",
      "updatedAt",
    ],
    include: [
      {
        model: Customers,
        as: "detail_info_customer",
      },
      {
        model: DetailOrderTickets,
        as: "detail_list_detail_order",
      },
    ],
    order: [
      ['date_welcome', 'DESC']
  ],
    where: where,
  })
    .then((result) =>
      res.json({
        total: result.count,
        data: result.rows,
      })
    )
    .catch((err) => {
      res.status(500).send("Internal server " + err);
    });
};
exports.getOrderTicketById = (req, res) => {
  let { id } = req.params;
  OrderTickets.findOne({
    attributes: [
      "code_order",
      "date_welcome",
      "date_leave",
      "status",
      "staffs_code_staff",
      "createdAt",
      "updatedAt",
    ],
    include: [
      {
        model: Customers,
        as: "detail_info_customer",
      },
      {
        model: DetailOrderTickets,
        as: "detail_list_detail_order",
        include: [
          {
            model: RankRooms,
            as: "detail_info_rank_room",
            include: [
              {
                model: KindRooms,
                as: "detail_info_kind_room",
              },
              {
                model: TypeRooms,
                as: "detail_info_type_room",
              },
            ],
          },
        ],
      },
    ],
    where: {
      code_order: id,
    },
  })
    .then((result) => res.json(result))
    .catch((err) => {
      res.status(500).send("Internal server " + err);
    });
};
exports.deleteOrderTicket = async (req, res) => {
  let { id } = req.body;
  await OrderTickets.destroy({
    where: {
      id: id,
    },
  })
    .then((result) => {
      res.status(200).send({
        deleted: id,
      });
    })
    .catch((err) => res.status(500).send("Internal Server"));
};
exports.acceptOrderTicket = async (req, res) => {
  let { staffs_code_staff } = req.body;
  let { id } = req.params;
  try {
    await OrderTickets.update(
      { staffs_code_staff, status: "1" },
      {
        where: {
          code_order: id,
        },
      }
    );
    let order = await OrderTickets.findOne({
      attributes: ["code_order"],
      include: [
        {
          model: Customers,
          as: "detail_info_customer",
        },
      ],
      where: {
        code_order: id,
      },
    });
    let img = await QRCode.toDataURL("" + id);
    var mail = {
      from: "mailsenderptithcm@gmail.com", // Địa chỉ email của người gửi
      to: order.detail_info_customer.email, // Địa chỉ email của người gửi
      subject: "HOTEL 5 STAR", // Tiêu đề mail
      text: "Bạn nhận được 1 QR", // plain text body
      attachments: [
        {
          filename: "qr" + ".jpg",
          contentType: "image/jpeg",
          content: new Buffer.from(img.split("base64,")[1], "base64"),
        },
      ],
    };
    transporter.sendMail(mail, function (error, info) {
      if (error) {
        // nếu có lỗi
        console.log(error);
      } else {
        //nếu thành công
        console.log("Email sent: " + info.response);
      }
    });
    res.status(200).send("success");
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.cancelOrder = async (req, res) => {
  let { id } = req.body;
  try {
    await OrderTickets.update(
      { status: "-1" },
      {
        where: {
          code_order: id,
        },
      }
    );
    await DetailStatus.update(
      {
        status_rooms_code_status: "PT",
      },
      {
        where: {
          order_tickets_code_order: id,
        },
      }
    );
    res.status(200).send("success");
  } catch (error) {
    res.status(500).send(error);
  }
};
function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener("load", () => callback(reader.result));
  reader.readAsDataURL(img);
}
exports.createQrCode = async (req, res) => {
  let { code } = req.body;
  try {
    let img = await QRCode.toDataURL("" + code);
    var mail = {
      from: "mailsenderptithcm@gmail.com", // Địa chỉ email của người gửi
      to: "phungdang98360@gmail.com", // Địa chỉ email của người gửi
      subject: "HOTEL 5 STAR", // Tiêu đề mail
      text: "Bạn nhận được 1 QR", // plain text body
      attachments: [
        {
          filename: "qr" + ".jpg",
          contentType: "image/jpeg",
          content: new Buffer.from(img.split("base64,")[1], "base64"),
        },
      ],
    };
    transporter.sendMail(mail, function (error, info) {
      if (error) {
        // nếu có lỗi
        console.log(error);
      } else {
        //nếu thành công
        console.log("Email sent: " + info.response);
      }
    });

    res.status(200).send(img);
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.getOrderTicketFromCustomer = async (req, res) => {
  let { identity_card } = req.query;
  try {
    let customer = await Customers.findOne({
      attributes: ["code_customer"],
      where: {
        code: identity_card,
      },
    });
    let result = await sequelize.query(
      `SELECT MAX(code_order) as max FROM order_tickets where customers_code_customer='${customer.code_customer}' and status=1 GROUP BY customers_code_customer`,
      { type: QueryTypes.SELECT }
    );
    let maxId = result[0].max;
    res.status(200).send({ max_id: maxId });
  } catch (error) {
    res.status(500).send(error);
  }
};

"use strict";
var express = require("express");
var router = express.Router();
var nodemailer = require("nodemailer");
// const option = {
//   service: "gmail",
//   auth: {
//     user: "mailsenderptithcm@gmail.com", // email hoặc username
//     pass: "ptithcm123", // password
//   },
// };
// var transporter = nodemailer.createTransport(option);
// transporter.verify(function (error, success) {
//   // Nếu có lỗi.
//   if (error) {
//     console.log(error);
//   } else {
//     //Nếu thành công.
//     console.log("Kết nối thành công!");
//   }
// });

router.get("/get-order-ticket", (req, res) => {
  OrderTickets.findAll({
    attributes: [
      "id",
      "code",
      "date_order",
      "amount_room",
      "date_welcome",
      "date_leave",
      "status",
      "createdAt",
      "updatedAt",
    ],
    include: [
      {
        model: Customers,
        as: "detail_info_customer",
      },
      {
        model: DetailOrderTickets,
        as: "detail_list_detail_order",
      },
    ],
  }).then((result) =>
    res.json({
      total: result.length,
      data: result,
    }),
  );
});
//history_exels
router.post("/history-exel", async (req, res) => {
  let { RankRoom, codeCustomer, dateWelcome, dateLeave } = req.body;
  let temp = [];
  for (let i = 0; i < RankRoom.length; i++) {
    temp.push({
      identity_card: codeCustomer,
      rank_room: RankRoom[i].rankRoomId,
      date_welcome: dateWelcome,
      date_leave: dateLeave,
    });
  }
  const t = await sequelize.transaction();
  try {
    await HistoryExels.bulkCreate(temp, { transaction: t });
    t.commit();
    res.status(200).send({ message: "success" });
  } catch (error) {
    t.rollback();
    res.status(500).send(error);
  }
});
router.post('/customer-exel',async (req, res)=>{
  let {date_welcome,date_leave,list_customer}=req.body;
  let temp = [];
  const t=await sequelize.transaction();
  try {
    for(let i = 0; i < list_customer.length; i++){
      temp.push({
        identity_card:list_customer[i].codeCustomer,
        date_welcome,
        date_leave
      })
      let customer=await Customers.findOne({
        where: {
          code: list_customer[i].codeCustomer
        }
      });
      if(customer) {
        await Customers.update(
          {
            firt_name: list_customer[i].firtName,
            last_name: list_customer[i].lastName,
            gender: list_customer[i].gender,
            address: list_customer[i].address,
            phone_number: list_customer[i].phone,
            email: list_customer[i].email,
          },
          {
            where: {
              code_customer: customer.code_customer,
            },
          },
        );
      }
      else {
        await Customers.create({
          code: list_customer[i].codeCustomer,
          firt_name: list_customer[i].firtName,
          last_name: list_customer[i].lastName,
          gender: list_customer[i].gender,
          address: list_customer[i].address,
          phone_number: list_customer[i].phone,
          email: list_customer[i].email,
          point: 0,
        });
      }
    }
    let customer_exel=await CustomerExels.bulkCreate(temp, {
      transaction: t,
    });
    await t.commit();
    res.status(200).send(customer_exel)
  } catch (error) {
    await t.rollback();
    res.status(500).send(error)
  }
})
//dat phong 2
router.post("/order-room", async (req, res) => {
  //const t = await sequelize.transaction();
  let orderIdTemp = "";
  let {
    codeCustomer,
    firtName,
    lastName,
    gender,
    address,
    phone,
    email,
    dateWelcome,
    dateLeave,
    status,
    RankRoom,
  } = req.body;
  let customers = await Customers.findAll({
    where: {
      code: codeCustomer,
    },
  });
  if (customers.length > 0) {
    const t = await sequelize.transaction();
    try {
      let result = await OrderTickets.create(
        {
          date_welcome: dateWelcome,
          date_leave: dateLeave,
          status: status,
          customers_code_customer: customers[0].code_customer,
        },
        { transaction: t },
      );
      let arrayDetail = [];
      orderIdTemp = result.code_order;
      for (let i = 0; i < RankRoom.length; i++) {
        arrayDetail.push({
          order_tickets_code_order: result.code_order,
          rank_rooms_id: RankRoom[i].rankRoomId,
          amount_room: RankRoom[i].amount,
        });
      }
      await DetailOrderTickets.bulkCreate(arrayDetail, { transaction: t });
      // var mail = {
      //   from: "mailsenderptithcm@gmail.com", // Địa chỉ email của người gửi
      //   to: email, // Địa chỉ email của người gửi
      //   subject: "HOTEL 5 STAR", // Tiêu đề mail
      //   text:
      //     "Your code order ticket " +orderIdTemp // Nội dung mail dạng text
      // };
      // transporter.sendMail(mail, function (error, info) {
      //   if (error) {
      //     // nếu có lỗi
      //     console.log(error);
      //   } else {
      //     //nếu thành công
      //     console.log("Email sent: " + info.response);
      //   }
      // });
      let arrStatusRoom = [];
      for (let i = 0; i < RankRoom.length; i++) {
        let temp = RankRoom[i].listRoom;
        for (let j = 0; j < temp.length; j++) {
          arrStatusRoom.push({
            time_start: dateWelcome,
            time_end: dateLeave,
            rooms_code_room: temp[j],
            status_rooms_code_status: "DT",
            order_tickets_code_order: orderIdTemp,
          });
        }
      }
      await DetailStatus.bulkCreate(arrStatusRoom, { transaction: t });
      await Customers.update(
        {
          firt_name: firtName,
          last_name: lastName,
          gender: gender,
          address: address,
          phone_number: phone,
          email: email,
        },
        {
          where: {
            code: codeCustomer,
          },
          transaction: t,
        },
      );
      await t.commit();
      let resultOrder = await OrderTickets.findOne({
        attributes: [
          "code_order",
          "date_welcome",
          "date_leave",
          "status",
          "createdAt",
          "updatedAt",
        ],
        include: [
          {
            model: Customers,
            as: "detail_info_customer",
          },
          {
            model: DetailOrderTickets,
            as: "detail_list_detail_order",
          },
        ],
        where: {
          code_order: orderIdTemp,
        },
      });
      res.status(200).send(resultOrder);
    } catch (error) {
      await t.rollback();
      res.status(500).send(error);
    }
  }
  //neu kh ko có trong csdl
  else {
    const t = await sequelize.transaction();
    try {
      let result = await Customers.create(
        {
          code: codeCustomer,
          firt_name: firtName,
          last_name: lastName,
          gender: gender,
          address: address,
          phone_number: phone,
          email: email,
          point: 0,
        },
        { transaction: t },
      );
      let resultOrder = await OrderTickets.create(
        {
          date_welcome: dateWelcome,
          date_leave: dateLeave,
          status: status,
          customers_code_customer: result.code_customer,
        },
        { transaction: t },
      );
      let arrayDetail = [];
      orderIdTemp = resultOrder.code_order;
      for (let i = 0; i < RankRoom.length; i++) {
        arrayDetail.push({
          order_tickets_code_order: resultOrder.code_order,
          rank_rooms_id: RankRoom[i].rankRoomId,
          amount_room: RankRoom[i].amount,
        });
      }
      await DetailOrderTickets.bulkCreate(arrayDetail, { transaction: t });
      let arrStatusRoom = [];
      for (let i = 0; i < RankRoom.length; i++) {
        let temp = RankRoom[i].listRoom;
        for (let j = 0; j < temp.length; j++) {
          arrStatusRoom.push({
            time_start: dateWelcome,
            time_end: dateLeave,
            rooms_code_room: temp[j],
            status_rooms_code_status: "DT",
            order_tickets_code_order: orderIdTemp,
          });
        }
      }
      await DetailStatus.bulkCreate(arrStatusRoom, { transaction: t });
      await t.commit();
      let orderTicket = await OrderTickets.findOne({
        attributes: [
          "code_order",
          "date_welcome",
          "date_leave",
          "status",
          "createdAt",
          "updatedAt",
        ],
        include: [
          {
            model: Customers,
            as: "detail_info_customer",
          },
          {
            model: DetailOrderTickets,
            as: "detail_list_detail_order",
          },
        ],
        where: {
          code_order: orderIdTemp,
        },
      });
      // var mail = {
      //   from: "mailsenderptithcm@gmail.com", // Địa chỉ email của người gửi
      //   to: email, // Địa chỉ email của người gửi
      //   subject: "HOTEL 5 STAR", // Tiêu đề mail
      //   text:
      //     "Your code order ticket " +
      //     orderTicket.code_order +
      //     ", Date welcome " +
      //     orderTicket.date_welcome, // Nội dung mail dạng text
      // };
      // transporter.sendMail(mail, function (error, info) {
      //   if (error) {
      //     // nếu có lỗi
      //     console.log(error);
      //   } else {
      //     //nếu thành công
      //     console.log("Email sent: " + info.response);
      //   }
      // });
      res.status(200).send(orderTicket);
    } catch (error) {
      await t.rollback();
      res.status(500).send(error);
    }
  }
});
module.exports = router;

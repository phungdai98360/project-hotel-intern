exports.getRankRoom = async (req, res) => {
  try {
    let result = await RankRooms.findAll({
      include: [
        {
          model: KindRooms,
          as: "detail_info_kind_room",
        },
        {
          model: TypeRooms,
          as: "detail_info_type_room",
        },
        {
          model: DetailPriceRooms,
          as: "detail_list_detail_price",
        },
      ],
    });
    res.status(200).send(result);
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.createRankRoom = async (req, res) => {
  let {
    kind_rooms_code_kind,
    type_rooms_code_type,
    price,
    date_apply,
    url_image
  } = req.body;
  const t = await sequelize.transaction();
  try {
    let rankRoom =await RankRooms.create(
      {
        url_image,
        kind_rooms_code_kind,
        type_rooms_code_type,
      },
      { transaction: t }
    );
    await DetailPriceRooms.create(
      { price, date_apply, rank_rooms_id: rankRoom.id },
      { transaction: t }
    );
    await t.commit();
    let result = await RankRooms.findOne({
      include: [
        {
          model: KindRooms,
          as: "detail_info_kind_room",
        },
        {
          model: TypeRooms,
          as: "detail_info_type_room",
        },
        {
          model: DetailPriceRooms,
          as: "detail_list_detail_price",
        },
      ],
      where: {
        id: rankRoom.id,
      },
    });
    res.status(200).send(result);
  } catch (error) {
    await t.rollback();
    res.status(500).send(error);
  }
};
exports.createPriceRoom= async (req, res) => {
    let {price, date_apply, rank_rooms_id}=req.body;
    try {
        await DetailPriceRooms.create(
            { price, date_apply, rank_rooms_id },
          );
          let result = await RankRooms.findOne({
            include: [
              {
                model: KindRooms,
                as: "detail_info_kind_room",
              },
              {
                model: TypeRooms,
                as: "detail_info_type_room",
              },
              {
                model: DetailPriceRooms,
                as: "detail_list_detail_price",
              },
            ],
            where: {
              id: rank_rooms_id,
            },
          });
          res.status(200).send(result);
    } catch (error) {
        res.status(500).send(error);
    }
}

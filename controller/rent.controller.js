const { QueryTypes } = require("sequelize");
const Op = Sequelize.Op;
exports.getDetailRent = async (req, res) => {
  let { id } = req.query;
  try {
    let result = await Rents.findOne({
      attributes: ["id"],
      include: [
        {
          model: Rooms,
          as: "detail_info_rooms",
          include: [
            {
              model: RankRooms,
              as: "detail_info_rankroom",
            },
          ],
        },
      ],
      where: {
        id: id,
      },
    });
    res.status(200).send(result);
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.getRoomByDeatilRent = async (req, res) => {
  let { code } = req.query;
  try {
    let resultDeatailRent = await sequelize.query(
      `SELECT MAX(id) as max FROM detail_rents where rooms_code_room=${code} GROUP BY rooms_code_room`,
      { type: QueryTypes.SELECT }
    );
    let maxId = resultDeatailRent[0].max;
    let result = await DetailCustomerAts.findAll({
      attributes: ["id"],
      include: [
        {
          model: Customers,
          as: "detail_info_customer",
        },
      ],
      where: {
        detail_rents_id: maxId,
      },
    });
    res.status(200).send(result);
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.getLimitPerson = async (req, res) => {
  try {
    let result = await RankRooms.findAll({
      attributes: ["id", "limit_person"],
    });
    res.status(200).send(result);
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.getCustomerBeingAt = async (req, res) => {
  try {
    let idDetailRent = await Rents.findAll({
      attributes: ["id", "rooms_code_room"],
      include: [
        {
          model: DetailCustomerAts,
          as: "detail_list_detail_rent",
          include: [
            {
              model: Customers,
              attributes: ["code"],
              as: "detail_info_customer",
            },
          ],
        },
      ],
      where: {
        bills_code_bill: {
          [Op.eq]: null,
        },
      },
    });
    let temp = [];
    for (let i = 0; i < idDetailRent.length; i++) {
      for (let j = 0; j < idDetailRent[i].detail_list_detail_rent.length; j++) {
        temp.push({
          room_id: idDetailRent[i].rooms_code_room,
          indenty_card:
            idDetailRent[i].detail_list_detail_rent[j].detail_info_customer
              .code,
        });
      }
    }
    res.status(200).send(temp);
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.updateDetailStatus = async (req, res) => {
  let { time_start, time_end, rooms_code_room, status } = req.body;
  try {
    let result = await DetailStatus.update(
      { status_rooms_code_status: status },
      {
        where: {
          time_start,
          time_end,
          rooms_code_room,
        },
      }
    );
    res.status(200).send(result);
  } catch (error) {
    res.status(500).send(error);
  }
};

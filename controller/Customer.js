"use strict";
var express=require("express");
var router=express.Router();
const { Op } = require("sequelize");
//const Customers = require("../models/customers");
router.get("/getall",(req,res)=>{
    Customers.findAll().then(customers=>{
        res.json({
          total: customers.length,
          data: customers,
        });
        //res.end();
    })
})
router.get("/get-by-id",(req,res)=>{
    let {id_card}=req.query;
    Customers.findOne({
        where: {
          code: id_card
        }
      }).then(customers=>{
        res.json(
          customers,
        );
    })
})
router.get("/get-by-code/:code",(req,res)=>{
  let {code}=req.params;
  Customers.findAll({
      where: {
        code: code
      }
    }).then(customers=>{
      res.json({
        total: customers.length,
        data: customers,
      });
  })
})
module.exports=router
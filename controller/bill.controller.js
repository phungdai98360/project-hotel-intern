const { QueryTypes } = require("sequelize");
const XlsxPopulate = require("xlsx-populate");
var moment = require("moment");
var path = require("path");
var cfg = require("./../config/src_config_index");
const Op = Sequelize.Op;
exports.checkRoomToBills = async (req, res) => {
  let { code_room } = req.query;
  let infor_customer = {};
  try {
    let resultDeatailRent = await sequelize.query(
      `SELECT MAX(id) as max FROM detail_rents where rooms_code_room='${code_room}' and bills_code_bill is null GROUP BY rooms_code_room`,
      { type: QueryTypes.SELECT }
    );
    let maxId = resultDeatailRent[0].max;
    let detailRent = await Rents.findOne({
      attributes: ["id", "createdAt", "payed", "rent_tickets_code_rent"],
      include: [
        {
          model: RentTickets,
          as: "detail_info_rent_ticket",
        },
      ],
      where: {
        id: maxId,
      },
    });
    let rentTicket = await RentTickets.findOne({
      attributes: ["code_rent"],
      include: [
        {
          model: OrderTickets,
          as: "detail_info_order",
          include: [
            {
              model: Customers,
              as: "detail_info_customer",
            },
          ],
        },
        {
          model: Customers,
          as: "detail_info_customer",
        },
      ],
      where: {
        code_rent: detailRent.rent_tickets_code_rent,
      },
    });
    if (rentTicket && rentTicket.detail_info_customer) {
      infor_customer = rentTicket.detail_info_customer;
    }
    if (
      rentTicket &&
      rentTicket.detail_info_order &&
      rentTicket.detail_info_order.detail_info_customer
    ) {
      infor_customer = rentTicket.detail_info_order.detail_info_customer;
    }
    let rents = await Rents.findAll({
      attributes: ["rooms_code_room"],
      where: {
        rent_tickets_code_rent: detailRent.detail_info_rent_ticket.code_rent,
      },
    });
    res.status(200).send({
      detail_rent: detailRent,
      list_rooms: rents,
      infor_customer,
    });
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.calculateRooms = async (req, res) => {
  let { code_room, quantity_custom } = req.query;
  let priceRoom = 0;
  let priceService = 0;
  let historyCalculate = [];
  let historyCalculateService = [];
  let sales = [];
  try {
    let resultDeatailRent = await sequelize.query(
      `SELECT MAX(id) as max FROM detail_rents where rooms_code_room='${code_room}' and bills_code_bill is null GROUP BY rooms_code_room`,
      { type: QueryTypes.SELECT }
    );
    let maxId = resultDeatailRent[0].max;
    let detailRent = await Rents.findOne({
      attributes: ["id", "createdAt", "payed"],
      include: [
        {
          model: Rooms,
          as: "detail_info_rooms",
          include: [
            {
              model: RankRooms,
              as: "detail_info_rankroom",
              include: [
                {
                  model: DetailPriceRooms,
                  as: "detail_list_detail_price",
                },
              ],
            },
          ],
        },
        {
          model: RentTickets,
          as: "detail_info_rent_ticket",
        },
        {
          model: DetailServices,
          as: "detail_list_detail_service",
          include: [
            {
              model: Services,
              as: "detail_info_service",
            },
          ],
        },
      ],
      where: {
        id: maxId,
      },
    });
    sales = await sequelize.query(
      `call get_sale("${moment(
        detailRent.detail_info_rent_ticket.time_welcome
      ).format("YYYY-MM-DD")}",${
        detailRent.detail_info_rooms.detail_info_rankroom.id
      })`
    );

    if (
      detailRent &&
      detailRent.detail_list_detail_service &&
      detailRent.detail_list_detail_service.length > 0
    ) {
      detailRent.detail_list_detail_service = detailRent.detail_list_detail_service.filter(
        (ser) => {
          return ser.payed === false;
        }
      );
      priceService = detailRent.detail_list_detail_service.reduce(
        (price, detail) => {
          return (price += parseInt(detail.price) * parseInt(detail.amount));
        },
        0
      );
    }

    if (
      detailRent &&
      detailRent.detail_list_detail_service &&
      detailRent.detail_list_detail_service.length > 0
    ) {
      for (let k = 0; k < detailRent.detail_list_detail_service.length; k++) {
        historyCalculateService.push({
          name:
            detailRent.detail_list_detail_service[k].detail_info_service.name,
          quantity: detailRent.detail_list_detail_service[k].amount,
          price: detailRent.detail_list_detail_service[k].price,
          date: moment(detailRent.detail_list_detail_service[k].createdAt),
        });
      }
    }

    let modeDate =
      moment(new Date()).diff(
        moment(detailRent.createdAt).format("YYYY-MM-DD"),
        "days"
      ) - quantity_custom;
    let listPrice =
      detailRent &&
      detailRent.detail_info_rooms &&
      detailRent.detail_info_rooms.detail_info_rankroom &&
      detailRent.detail_info_rooms.detail_info_rankroom.detail_list_detail_price
        ? detailRent.detail_info_rooms.detail_info_rankroom
            .detail_list_detail_price
        : [];
    let priceApply = listPrice.filter((lp) => {
      return moment(lp.date_apply).isBetween(
        moment(detailRent.createdAt).format("YYYY-MM-DD"),
        moment(new Date()).format("YYYY-MM-DD")
      );
    });
    let priceApplyAfter = listPrice.filter((lp) => {
      return moment(lp.date_apply).isAfter(
        moment(new Date()).format("YYYY-MM-DD")
      );
    });
    if (priceApply.length === 0) {
      let a = moment(new Date());
      let b = moment(detailRent.createdAt).format("YYYY-MM-DD");
      let betweenDate = a.diff(b, "days") - parseInt(modeDate);
      priceRoom =
        parseInt(betweenDate) *
        parseInt(
          listPrice[listPrice.length - (1 + priceApplyAfter.length)].price
        );
      historyCalculate.push({
        before: moment(detailRent.createdAt).format("YYYY-MM-DD HH:mm"),
        after: moment(new Date()).format("YYYY-MM-DD HH:mm"),
        price_apply:
          listPrice[listPrice.length - (1 + priceApplyAfter.length)].price,
        quantity: betweenDate,
      });
    }
    if (priceApply.length > 0) {
      //console.log(moment(priceApply[0].date_apply).format("YYYY-MM-DD"),moment(new Date()).format("YYYY-MM-DD"))
      if (moment(priceApply[0].date_apply).isSame(moment(new Date()), "day")) {
        let a = moment(new Date());
        let b = moment(detailRent.createdAt).format("YYYY-MM-DD");
        let betweenDate = a.diff(b, "days") - parseInt(modeDate);
        priceRoom =
          parseInt(betweenDate) *
          parseInt(
            listPrice[listPrice.length - (2 + priceApplyAfter.length)].price
          );
        historyCalculate.push({
          before: moment(detailRent.createdAt).format("YYYY-MM-DD HH:mm"),
          after: moment(new Date()).format("YYYY-MM-DD HH:mm"),
          price_apply:
            listPrice[listPrice.length - (2 + priceApplyAfter.length)].price,
          quantity: betweenDate,
        });
      } else {
        let a = moment(priceApply[0].date_apply);
        let b = moment(detailRent.createdAt).format("YYYY-MM-DD");
        let quantity1 = a.diff(b, "days");
        priceRoom =
          parseInt(quantity1) *
          parseInt(
            listPrice[
              listPrice.length -
                (1 + priceApplyAfter.length + priceApply.length)
            ].price
          );
        historyCalculate.push({
          before: moment(detailRent.createdAt).format("YYYY-MM-DD HH:mm"),
          after: moment(priceApply[0].date_apply).format("YYYY-MM-DD 12:00"),
          price_apply:
            listPrice[
              listPrice.length -
                (1 + priceApplyAfter.length + priceApply.length)
            ].price,
          quantity: quantity1,
        });
        for (let i = 0; i < priceApply.length; i++) {
          if (i === priceApply.length - 1) {
            priceRoom +=
              parseInt(
                moment(new Date()).diff(
                  moment(priceApply[i].date_apply).format("YYYY-MM-DD"),
                  "day"
                ) - parseInt(modeDate)
              ) * parseInt(priceApply[i].price);
            historyCalculate.push({
              before: moment(priceApply[i].date_apply).format(
                "YYYY-MM-DD 12:00"
              ),
              after: moment(new Date()).format("YYYY-MM-DD HH:mm"),
              price_apply: priceApply[i].price,
              quantity: parseInt(
                moment(new Date()).diff(
                  moment(priceApply[i].date_apply).format("YYYY-MM-DD"),
                  "day"
                ) - parseInt(modeDate)
              ),
            });
          } else {
            priceRoom +=
              parseInt(
                moment(priceApply[i + 1].date_apply).diff(
                  moment(priceApply[i].date_apply).format("YYYY-MM-DD"),
                  "day"
                )
              ) * parseInt(priceApply[i].price);
            historyCalculate.push({
              before: moment(priceApply[i].date_apply).format(
                "YYYY-MM-DD HH:mm"
              ),
              after: moment(priceApply[i + 1].date_apply).format(
                "YYYY-MM-DD HH:mm"
              ),
              price_apply: priceApply[i].price,
              quantity: parseInt(
                moment(priceApply[i + 1].date_apply).diff(
                  moment(priceApply[i].date_apply).format("YYYY-MM-DD"),
                  "day"
                )
              ),
            });
          }
        }
      }
    }

    let priceRoomSale = priceRoom;
    if (sales.length > 0) {
      priceRoomSale = priceRoom - (priceRoom * sales[0].ratio) / 100;
    }
    res.status(200).send({
      priceRoom,
      priceRoomSale,
      historyCalculate,
      priceService,
      historyCalculateService,
      sales,
    });
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.createBill = async (req, res) => {
  let {
    code_bill,
    rent_tickets_code_rent,
    staffs_code_staff,
    price_room,
    price_service,
    list_rooms,
  } = req.body;
  const t = await sequelize.transaction();
  let idCustomer = 0;
  let point = 0;
  try {
    let rentTicket = await RentTickets.findOne({
      attributes: ["code_rent"],
      include: [
        {
          model: Customers,
          as: "detail_info_customer",
        },
        {
          model: Rents,
          as: "detail_list_rents",
        },
        {
          model: OrderTickets,
          as: "detail_info_order",
          include: [
            {
              model: Customers,
              as: "detail_info_customer",
            },
          ],
        },
      ],
      where: {
        code_rent: rent_tickets_code_rent,
      },
    });
    if (rentTicket && rentTicket.detail_info_customer) {
      idCustomer = rentTicket.detail_info_customer.code_customer;
      point = rentTicket.detail_info_customer.point;
    }
    if (
      rentTicket &&
      rentTicket.detail_info_order &&
      rentTicket.detail_info_order.detail_info_customer &&
      rentTicket.detail_info_order.customers_code_customer
    ) {
      idCustomer = rentTicket.detail_info_order.customers_code_customer;
      point = rentTicket.detail_info_order.detail_info_customer.point;
    }
    let bill = await Bills.create({
      code_bill,
      rent_tickets_code_rent,
      staffs_code_staff,
    });
    for (let i = 0; i < list_rooms.length; i++) {
      let resultDeatailStatus = await sequelize.query(
        `SELECT MAX(id) as max FROM detail_status WHERE status_rooms_code_status='DO' AND rooms_code_room='${list_rooms[i].code_room}' GROUP BY rooms_code_room`,
        { type: QueryTypes.SELECT }
      );
      let maxId = resultDeatailStatus[0].max;
      await DetailStatus.update(
        {
          status_rooms_code_status: "PT",
        },
        {
          where: {
            id: maxId,
          },
          transaction: t,
        }
      );
    }
    let total = parseInt(price_room) + parseInt(price_service);
    point += Math.floor(parseInt(total) / 50000);
    await Bills.update(
      {
        price_room,
        price_service,
        total_price: parseInt(price_room) + parseInt(price_service),
      },
      {
        where: {
          code_bill: bill.code_bill,
        },
        transaction: t,
      }
    );
    for (let i = 0; i < list_rooms.length; i++) {
      await Rents.update(
        {
          surcharge: list_rooms[i].surcharge,
          bills_code_bill: bill.code_bill,
          payed: 1,
        },
        {
          where: {
            rooms_code_room: list_rooms[i].code_room,
            rent_tickets_code_rent: rent_tickets_code_rent,
          },
          transaction: t,
        }
      );
      let rent = await Rents.findOne({
        attributes: ["id"],
        where: {
          rooms_code_room: list_rooms[i].code_room,
          rent_tickets_code_rent: rent_tickets_code_rent,
        },
      });
      await DetailServices.update(
        { bills_code_bill: bill.code_bill, payed: 1 },
        {
          where: {
            detail_rents_id: rent.id,
            payed: 0,
          },
          transaction: t,
        }
      );
    }
    if (rentTicket.detail_list_rents.length === list_rooms.length) {
      await RentTickets.update(
        {
          payed: 1,
        },
        {
          where: {
            code_rent: rent_tickets_code_rent,
          },
          transaction: t,
        }
      );
    }
    await Customers.update(
      {
        point,
      },
      {
        where: {
          code_customer: idCustomer,
        },
        transaction: t,
      }
    );
    await t.commit();
    res.status(200).send("success");
  } catch (error) {
    await t.rollback();
    res.status(500).send(error);
  }
};
exports.getBills = async (req, res) => {
  let {
    perPage,
    page,
    created_from,
    created_to,
    staff_id,
    download,
    token,
  } = req.query;
  let createdAt = {};
  if (created_from && created_to) {
    createdAt = {
      [Op.between]: [created_from, moment(created_to).endOf("day")],
    };
  }
  if (created_from && !created_to) {
    createdAt = {
      [Op.between]: [created_from, new Date()],
    };
  }
  if (!created_from && created_to) {
    createdAt = {
      [Op.lte]: moment(created_to).endOf("day"),
    };
  }
  let where = {
    createdAt: createdAt,
    staffs_code_staff: staff_id,
  };

  if (!created_from && !created_to) {
    delete where.createdAt;
  }
  if (!staff_id) {
    delete where.staffs_code_staff;
  }
  let paging = {};
  if (page && perPage && !download) {
    paging.limit = parseInt(perPage);
    paging.offset = parseInt(page - 1) * parseInt(perPage);
  }
  try {
    let result = await Bills.findAndCountAll({
      ...paging,
      distinct: true,
      attributes: [
        "code_bill",
        "price_room",
        "price_service",
        "total_price",
        "staffs_code_staff",
        "createdAt",
      ],
      include: [
        {
          model: Staffs,
          as: "detail_info_staff",
        },
        {
          model: RentTickets,
          as: "detail_info_rent_ticket",
          include: [
            {
              model: OrderTickets,
              as: "detail_info_order",
              include: [
                {
                  model: Customers,
                  as: "detail_info_customer",
                },
              ],
            },
            {
              model: Customers,
              as: "detail_info_customer",
            },
          ],
        },
      ],
      where: where,
    });
    if (download) {
      return await exportBills(res, req, result.rows);
    }
    res.status(200).send(result);
  } catch (error) {
    res.status(500).send(error);
  }
};
async function exportBills(res, req, dataSend) {
  let template = "DanhSachHoaDon.xlsx";
  let name_template = "_DanhSachHoaDon.xlsx";
  let dataOutput = await XlsxPopulate.fromFileAsync(
    path.join(__dirname, template)
  ).then((workbook) => {
    let query = req.query;
    let sheet = workbook.sheet("Sheet1");
    sheet.cell("C2").value(`Người xuất: ${query.created_by}`);
    sheet
      .cell("C3")
      .value(`Thời gian xuất file: ${moment().format("HH:mm:ss DD/MM/YYYY")}`);
    sheet
      .cell("C4")
      .value(`Xuất file từ link : http://localhost:4200/${query.link || ""}`);
    writeData(dataSend, sheet);
    return workbook.outputAsync();
  });
  let filename = moment().format("YYYYMMDD-HHmmss") + name_template;
  res.setHeader("Content-Disposition", "attachment; filename=" + filename);
  res.setHeader(
    "Content-Type",
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  );
  res.send(dataOutput);
}
function renderCustomer(inforRentTicket) {
  if (inforRentTicket.detail_info_customer) {
    return (
      inforRentTicket.detail_info_customer.firt_name +
      " " +
      inforRentTicket.detail_info_customer.last_name
    );
  }
  if (
    inforRentTicket.detail_info_order &&
    inforRentTicket.detail_info_order.detail_info_customer
  ) {
    return (
      inforRentTicket.detail_info_order.detail_info_customer.firt_name +
      " " +
      inforRentTicket.detail_info_order.detail_info_customer.last_name
    );
  }
};
function writeData(dataSend, sheet) {
  dataSend.map((data, x) => {
    sheet.cell(`A${x + 7}`).value(x + 1);
    sheet.cell(`B${x + 7}`).value(data.code_bill);
    sheet.cell(`C${x + 7}`).value(renderCustomer(data.detail_info_rent_ticket));
    sheet.cell(`D${x + 7}`).value(data.price_room);
    sheet.cell(`E${x + 7}`).value(data.price_service);
    sheet
      .cell(`F${x + 7}`)
      .value(data.total_price - (data.price_service + data.price_room));
    sheet.cell(`G${x + 7}`).value(data.total_price);
    sheet
      .cell(`H${x + 7}`)
      .value(moment(data.createdAt).format("DD-MM-YYYY HH:mm"));
  });
}

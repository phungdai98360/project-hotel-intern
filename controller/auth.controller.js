const config = require("../config/auth.config");

const Op = Sequelize.Op;
const { QueryTypes } = require("sequelize");
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
var moment = require("moment");

exports.signup = (req, res) => {
  // Save User to Database
  let {
    code_staff,
    identy_card,
    firt_name,
    last_name,
    gender,
    address,
    phone_number,
    email,
    parts_code_part,
    password,
    role,
  } = req.body;
  Staffs.create({
    code_staff,
    identy_card,
    firt_name,
    last_name,
    gender,
    address,
    phone_number,
    parts_code_part,
    role,
    email: email,
    password: bcrypt.hashSync(password, 8),
    deleted: 0,
  })
    .then((user) => {
      Staffs.findAll({
        attributes: [
          "code_staff",
          "firt_name",
          "last_name",
          "gender",
          "address",
          "phone_number",
          "email",
          "role",
          "createdAt",
          "updatedAt",
          "deleted",
        ],
        include: [
          {
            model: Part,
            as: "detail_info_part",
          },
        ],
      }).then((result) =>
        res.status(200).send({ total: result.length, data: result })
      );
    })
    .catch((err) => {
      res.status(500).send({ message: err.message });
    });
};
exports.updateStaff = async (req, res) => {
  let {
    code_staff,
    identy_card,
    firt_name,
    last_name,
    gender,
    address,
    phone_number,
    email,
    part_id,
    password,
    role,
  } = req.body;
  if (password) {
    await Staffs.update(
      {
        identy_card,
        firt_name,
        last_name,
        gender,
        address,
        phone_number,
        email,
        part_id,
        password: bcrypt.hashSync(password, 8),
        role,
      },
      {
        where: {
          code_staff: code_staff,
        },
      }
    );
  } else {
    await Staffs.update(
      {
        identy_card,
        firt_name,
        last_name,
        gender,
        address,
        phone_number,
        email,
        part_id,
        role,
      },
      {
        where: {
          code_staff: code_staff,
        },
      }
    );
  }
  let result = await Staffs.findOne({
    include: [
      {
        model: Part,
        as: "detail_info_part",
      },
    ],
    where: {
      code_staff: code_staff,
    },
  });
  res.status(200).send(result);
};
exports.deleteStaff = async (req, res) => {
  let { parts_code_part, status_delete } = req.body;
  let result = await Staffs.update(
    { deleted: status_delete },
    {
      where: {
        parts_code_part: parts_code_part,
      },
    }
  );
  console.log(result);
  res.status(200).send({
    deleted: parts_code_part,
  });
};
// res.status(200).send({ message: "User was registered successfully!", //     user });
exports.login = (req, res) => {
  let { email, password } = req.body;
  Staffs.findOne({
    attributes: [
      "code_staff",
      "email",
      "firt_name",
      "last_name",
      "password",
      "role",
      "createdAt",
      "updatedAt",
      "deleted",
    ],
    where: {
      email: email,
      deleted: 0,
    },
  })
    .then((user) => {
      if (!user) {
        return res.status(404).send({ message: "User Not found." });
      }

      var passwordIsValid = bcrypt.compareSync(password, user.password);

      if (!passwordIsValid) {
        return res.status(401).send({
          accessToken: null,
          message: "Invalid Password!",
        });
      }

      var token = jwt.sign({ id: user.email }, config.secret, {
        expiresIn: "30d", // 24 hours
      });

      res.status(200).send({
        code: user.code_staff,
        email: user.email,
        name: user.firt_name + " " + user.last_name,
        roles: user.role,
        accessToken: token,
      });
    })
    .catch((err) => {
      res.status(500).send({ message: err.message });
    });
};
exports.getRoomRandom = (req, res) => {
  let { id } = req.params;
  OrderTickets.findOne({
    attributes: [
      "code_order",
      "date_welcome",
      "date_leave",
      "status",
      "createdAt",
      "updatedAt",
    ],
    include: [
      {
        model: Customers,
        as: "detail_info_customer",
      },
      {
        model: DetailOrderTickets,
        as: "detail_list_detail_order",
      },
      {
        model: RentTickets,
        as: "detail_list_rent",
      },
      {
        model: DetailStatus,
        as: "detail_list_detailstatus",
        include: [
          {
            model: StatusRooms,
            as: "detail_info_status",
          },
          {
            model: Rooms,
            as: "detail_info_room",
            include: [
              {
                model: RankRooms,
                as: "detail_info_rankroom",
                include: [
                  {
                    model: KindRooms,
                    as: "detail_info_kind_room",
                  },
                  {
                    model: TypeRooms,
                    as: "detail_info_type_room",
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
    where: {
      code_order: id,
    },
  }).then((result) => res.json(result));
};
//create rooms
exports.createRoom = (req, res) => {
  let { code_room, rank_rooms_id, floor } = req.body;
  Rooms.create({
    code_room,
    floor,
    rank_rooms_id,
  }).then((result) => res.status(200).send(result));
};
//crete rent ticket
exports.createRentTicket = async (req, res) => {
  let {
    code_rent,
    time_welcome,
    date_leave,
    payed,
    order_tickets_code_order,
    staffs_code_staff,
  } = req.body;
  try {
    let result = await RentTickets.create({
      code_rent,
      time_welcome,
      date_leave,
      payed,
      order_tickets_code_order,
      staffs_code_staff,
    });
    await OrderTickets.update(
      { status: "2" },
      {
        where: {
          code_order: order_tickets_code_order,
        },
      }
    );
    res.status(200).send(result);
  } catch (error) {
    res.status(500).send(error);
  }
};
//give room 2
exports.giveRooms = async (req, res) => {
  let { rent_ticket_id, room_id } = req.body;
  const t = await sequelize.transaction();
  try {
    let resultRentTicket = await RentTickets.findOne({
      where: {
        code_rent: rent_ticket_id,
      },
    });
    let sql = `call update_status_room('${room_id}','${moment(
      resultRentTicket.time_welcome
    ).format("YYYY-MM-DD")}','${moment(resultRentTicket.date_leave).format(
      "YYYY-MM-DD"
    )}')`;
    const idStatus = await sequelize.query(sql);
    let result = await Rents.create(
      {
        rent_tickets_code_rent: rent_ticket_id,
        rooms_code_room: room_id,
        payed: 0,
      },
      { transaction: t }
    );
    if (idStatus[0]) {
      await DetailStatus.update(
        {
          status_rooms_code_status: "DO",
        },
        {
          where: {
            id: idStatus[0].id,
          },
          transaction: t,
        }
      );
    } else {
      await DetailStatus.create(
        {
          time_start: moment(resultRentTicket.time_welcome).format(
            "YYYY-MM-DD"
          ),
          time_end: moment(resultRentTicket.date_leave).format("YYYY-MM-DD"),
          rooms_code_room: room_id,
          status_rooms_code_status: "DO",
        },
        { transaction: t }
      );
    }
    await t.commit();
    res.status(200).send(result);
  } catch (error) {
    console.log(error);
    await t.rollback();
    res.status(500).send(error);
  }
};
exports.detailCustomerAt = async (req, res) => {
  let { detail_rents_id, list_customer } = req.body;
  let temp = [];
  const t = await sequelize.transaction();
  try {
    if (list_customer.length > 0) {
      for (let i = 0; i < list_customer.length; i++) {
        let customer = await Customers.findOne({
          where: {
            code: list_customer[i].codeCustomer,
          },
        });
        if (customer) {
          await Customers.update(
            {
              firt_name: list_customer[i].firtName,
              last_name: list_customer[i].lastName,
              gender: list_customer[i].gender,
              address: list_customer[i].address,
              phone_number: list_customer[i].phone,
              email: list_customer[i].email,
            },
            {
              where: {
                code_customer: customer.code_customer,
              },
            }
          );
          temp.push({
            detail_rents_id,
            customers_code_customer: customer.code_customer,
          });
        } else {
          let createCustomer = await Customers.create({
            code: list_customer[i].codeCustomer,
            firt_name: list_customer[i].firtName,
            last_name: list_customer[i].lastName,
            gender: list_customer[i].gender,
            address: list_customer[i].address,
            phone_number: list_customer[i].phone,
            email: list_customer[i].email,
            point: 0,
          });
          temp.push({
            detail_rents_id,
            customers_code_customer: createCustomer.code_customer,
          });
        }
      }
    } else {
      temp = null;
    }
    let detailCustomerAt = await DetailCustomerAts.bulkCreate(temp, {
      transaction: t,
    });
    await t.commit();
    res.status(200).send(detailCustomerAt);
  } catch (error) {
    await t.rollback();
    res.status(500).send(error);
  }
};
exports.createRentTicketByCustomer = async (req, res) => {
  let {
    codeCustomer,
    firtName,
    lastName,
    gender,
    address,
    phone,
    email,
    time_welcome,
    date_leave,
    staffs_code_staff,
    payed,
    code_rent,
  } = req.body;
  let customer = await Customers.findOne({
    where: {
      code: codeCustomer,
    },
  });
  if (customer) {
    try {
      await Customers.update(
        {
          firt_name: firtName,
          last_name: lastName,
          gender: gender,
          address: address,
          phone_number: phone,
          email: email,
        },
        {
          where: {
            code: codeCustomer,
          },
        }
      );
      let result = await RentTickets.create({
        code_rent,
        time_welcome,
        date_leave,
        payed,
        staffs_code_staff,
        customers_code_customer: customer.code_customer,
      });
      res.status(200).send(result);
    } catch (error) {
      res.status(500).send(error);
    }
  } else {
    try {
      let customerCreate = await Customers.create({
        code: codeCustomer,
        firt_name: firtName,
        last_name: lastName,
        gender: gender,
        address: address,
        phone_number: phone,
        email: email,
        point: 0,
      });
      let result = await RentTickets.create({
        code_rent,
        time_welcome,
        date_leave,
        payed,
        staffs_code_staff,
        customers_code_customer: customerCreate.code_customer,
      });
      res.status(200).send(result);
    } catch (error) {
      res.status(500).send(error);
    }
  }
};
//service
exports.getAllService = (req, res) => {
  Services.findAll({
    include: [
      {
        model: DetailPriceServices,
        as: "detail_list_detail_price",
        where: {
          id: {
            [Sequelize.Op.in]: [
              Sequelize.literal(
                "SELECT MAX(id) FROM detail_price_services GROUP BY services_code"
              ),
            ],
          },
        },
      },
    ],
  })
    .then((result) =>
      res.status(200).send({
        total: result.length,
        data: result,
      })
    )
    .catch((err) =>
      res.status(500).send({
        message: "Internal server",
        err: err,
      })
    );
};
exports.createService = async (req, res) => {
  const t = await sequelize.transaction();
  let { name, price, date_apply } = req.body;
  try {
    let result = await Services.create(
      {
        name,
      },
      { transaction: t }
    );
    let detail_price = await DetailPriceServices.create(
      {
        price,
        date_apply,
        services_code: result.code,
      },
      { transaction: t }
    );
    await t.commit();
    let service = await Services.findOne({
      include: [
        {
          model: DetailPriceServices,
          as: "detail_list_detail_price",
          where: {
            id: {
              [Sequelize.Op.in]: [
                Sequelize.literal(
                  "SELECT MAX(id) FROM detail_price_services GROUP BY services_code"
                ),
              ],
            },
          },
        },
      ],
      where: {
        code: result.code,
      },
    });
    res.status(200).send(service);
  } catch (error) {
    await t.rollback();
    res.status(500).send({
      message: error.message,
    });
  }
};
exports.updateService = async (req, res) => {
  let { code, name, price, date_apply } = req.body;
  let service = {};
  try {
    await Services.update({ name }, { where: { code } });
    if (price && date_apply) {
      await DetailPriceServices.create({
        price,
        date_apply,
        services_code: code,
      });
    }
    service = await Services.findOne({
      include: [
        {
          model: DetailPriceServices,
          as: "detail_list_detail_price",
          where: {
            id: {
              [Sequelize.Op.in]: [
                Sequelize.literal(
                  "SELECT MAX(id) FROM detail_price_services GROUP BY services_code"
                ),
              ],
            },
          },
        },
      ],
      where: {
        code: code,
      },
    });
    res.status(200).send(service);
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.createDetailService = async (req, res) => {
  let {
    idRoom,
    idService,
    amount,
    price,
    payed,
    code_bill,
    code_staff,
  } = req.body;
  let tempPrice = 0;
  let bill2 = {};
  try {
    let resultDeatailRent = await sequelize.query(
      `SELECT MAX(id) as max FROM detail_rents where rooms_code_room=${idRoom} GROUP BY rooms_code_room`,
      { type: QueryTypes.SELECT }
    );
    let maxId = resultDeatailRent[0].max;
    //abc
    if (code_bill) {
      let detailRent = await Rents.findOne({
        attributes: ["id", "createdAt", "payed", "rent_tickets_code_rent"],
        where: {
          id: maxId,
        },
      });
      let bill = await Bills.findOne({
        where: {
          code_bill: code_bill,
        },
      });
      if (!bill || Object.keys(bill).length === 0) {
        await Bills.create({
          code_bill,
          rent_tickets_code_rent: detailRent.rent_tickets_code_rent,
          staffs_code_staff: code_staff,
          price_room: 0,
          price_service: 0,
          total_price: 0,
        });
      }
      bill2 = await Bills.findOne({
        where: {
          code_bill: code_bill,
        },
      });
    }
    //abc
    await DetailServices.create({
      services_code: idService,
      detail_rents_id: maxId,
      amount: amount,
      price,
      payed: payed ? payed : 0,
      bills_code_bill: code_bill ? code_bill : null,
    });

    tempPrice +=
      (bill2.price_service ? bill2.price_service : 0) +
      parseInt(price) * parseInt(amount);
    if (code_bill) {
      await Bills.update(
        {
          price_service: tempPrice,
          total_price: tempPrice,
        },
        {
          where: {
            code_bill: code_bill,
          },
        }
      );
    }
    res.status(200).send("Successfully created");
  } catch (error) {
    res.status(500).send(error);
  }
};
//staff
exports.getAllStaff = (req, res) => {
  Staffs.findAll({
    include: [
      {
        model: Part,
        as: "detail_info_part",
      },
    ],
  }).then((result) =>
    res.status(200).send({
      total: result.length,
      data: result,
    })
  );
};
//part
exports.getPart = (req, res) => {
  Part.findAll()
    .then((result) => res.status(200).send(result))
    .catch((err) => res.status(500).send("Internal server"));
};
//change room
exports.changeRoom = async (req, res) => {
  let { id_rent_ticket, id_room_old, id_room_new } = req.body;
  let rentTicket = await RentTickets.findOne({
    where: {
      id: id_rent_ticket,
    },
  });
  let rent = await Rents.findOne({
    where: {
      rent_ticket_id: id_rent_ticket,
      room_id: id_room_old,
    },
  });
  let detailStatus = await DetailStatus.findOne({
    where: {
      rooms_id: id_room_old,
      time_start: moment(rentTicket.time_welcome).format("YYYY-MM-DD"),
      time_end: moment(rentTicket.date_leave).format("YYYY-MM-DD"),
    },
  });
  await Rents.update(
    { room_id: id_room_new },
    {
      where: {
        id: rent.id,
      },
    }
  );
  await DetailStatus.update(
    { rooms_id: id_room_new },
    {
      where: {
        id: detailStatus.id,
      },
    }
  );
  res.status(200).send("success");
};
exports.checkChangeRom = async (req, res) => {
  let { rent_id, room_id } = req.query;
  let rent_ticket = await RentTickets.findOne({
    where: {
      id: rent_id,
    },
  });
  let room = await Rooms.findOne({
    include: [
      {
        model: RankRooms,
        as: "detail_info_rankroom",
      },
    ],
    where: {
      id: room_id,
    },
  });
  let sql = `call load_rooms_empty(${room.detail_info_rankroom.id},'${moment(
    rent_ticket.time_welcome
  ).format("YYYY-MM-DD")}','${moment(rent_ticket.date_leave).format(
    "YYYY-MM-DD"
  )}')`;
  let arrRoomChange = await sequelize.query(sql);
  res.status(200).send(arrRoomChange);
};
exports.revenueFromDateToDate = (req, res) => {
  let { from, to } = req.query;
  let param = {
    from: moment(new Date()).format("YYYY-MM-DD"),
    to: moment(moment().subtract(7, "days")).format("YYYY-MM-DD"),
  };
  res.status(200).send(param);
};
exports.dashboardBill = async (req, res) => {
  let { from_date, to_date } = req.query;
  let where = {
    createdAt: {
      [Op.between]: [from_date, moment(to_date).endOf("day")],
    },
  };
  try {
    let result = await Bills.findAll({
      attributes: ["total_price", "createdAt"],
      where: where,
    });
    let total = result.reduce((count, r) => {
      return (count += r.total_price);
    }, 0);
    res.status(200).send({ total: total, result });
  } catch (error) {
    res.status(500).send(error);
  }
};
